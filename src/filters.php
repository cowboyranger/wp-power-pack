<?php
namespace Fresh;

if ( !function_exists('add_filter') ) {
  return;
}

/**
 * Move Yoast SEO metabox to below ACF Metabox
 */
add_filter( 'wpseo_metabox_prio', function() {
  return 'low';
});

/**
 * Capitalize default soberwp/models directory
 */
add_filter( 'sober/models/path', function() {
  return dirname(get_template_directory()) . '/app/Models';
});
