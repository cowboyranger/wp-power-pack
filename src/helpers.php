<?php
namespace Fresh;
use function App\config;
use function apply_filters;

if ( !function_exists('add_filter') ) {
  return;
}

/**
 * Simple function to pretty up our field partial includes.
 *
 * @param  mixed $partial
 * @return mixed
 */
function get_field_partial( $partial ) {
  $partial = str_replace('.', '/', $partial);

  return include( config('theme.dir')."/app/Fields/{$partial}.php" );
}

/**
 * Convert string to slug, replace non-latin with dash (-)
 *
 * @param   string    $string         String to convert
 * @return  string                    Converted string
 */
function slugify( $string ) {
  return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
}

/**
 * Generate <a> link from ACF Link field
 *
 * @param  array  $link_object   Array from Link field
 * @param  array  $link_attr     Pass in attributes with values that append to the <a> link
 * @return string                HTML <a> element from given $link_object
 */
function acf_link( $link_object, $link_attr = "" ) {
  $cta = "";

  $link_object = apply_filters( 'fresh/acf_link_object', $link_object );
  $link_attr = apply_filters( 'fresh/acf_link_attr', $link_attr );

  if( $link_object && isset( $link_object['target'] ) && isset( $link_object['title'] ) ) {
    $website_url = $link_object ? $link_object['url'] : '';
    $website_target = $link_object['target'] ? 'target="' . $link_object['target'] . '" rel="noopener"' : '';
    $website_text = $link_object['title'] ? $link_object['title'] : __("Learn more");

    // Loop through our attributes and prepare them for the markup
    $attr = '';
    if( $link_attr ) {
      $end = end($link_attr);
      foreach( $link_attr as $name => $value ) {
        if ( $end != $value ) {
          $attr  .= $name .'="'. $value . '" ';
        } else {
          $attr  .= $name .'="'. $value . '"';
        }
      }
    }

    if( $website_url ){
      $cta =  '<a href="' . esc_url( $website_url ) . '" ' . $website_target . ' ' . $attr . '>' . $website_text . '</a>';
    }
  }

  return $cta;
}
