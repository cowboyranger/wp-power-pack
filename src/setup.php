<?php
namespace Fresh;

use StoutLogic\AcfBuilder\FieldsBuilder;
use function App\config;
use function App\sage;

if ( !function_exists('add_action') ) {
  return;
}

add_action( 'after_setup_theme', function() {
  if ( function_exists('App\sage') ) {
    sage('blade')->compiler()->directive('acflink', function ($args) {
      return "<?php echo " . __NAMESPACE__ . "\\acf_link({$args}); ?>";
    });
  }
}, 20 );

/**
 * Initialize ACF Builder
 */
add_action( 'init', function () {
  collect(glob(config('theme.dir').'/app/Fields/*.php'))->map(function ($field) {
    return require_once($field);
  })->map(function ($field) {
    if ($field instanceof FieldsBuilder) {
      acf_add_local_field_group($field->build());
    }
  });
} );

/**
 * Register default theme option page for ACF
 */
if ( apply_filters( 'fresh/enable_theme_option', true ) && function_exists('acf_add_options_page') ) {
  /**
   * ACF Options Page
   */
  acf_add_options_page(array(
    'page_title'  => 'Theme Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_pages',
    'redirect'    => false
  ));
}
