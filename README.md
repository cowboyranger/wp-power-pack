# Fresh WordPress PowerPack
A bundle of handful utilities and settings to jump start WordPress development with Sage 9

## Install
```
$ composer require wp63/fresh-wp-powerpack
```

## Installed packages
* soberwp/models
* stoutlogic/acf-builder
* log1x/sage-directives

## Helper functions
* `acf_link()` with `@acflink` blade directive
* `slugify()`
* `get_field_partial()`

## Directories
Some directory are renamed to follow PSR-4 naming scheme
* soberwp/models directory `app/models` to `app/Models`
* stoutlogic/acf-builder `app/fields` to `app/Fields`

## Predefined settings
* Move YoastSEO meta box to below ACF fields set
* Predefined theme options page (slug: `theme-general-settings`). To disable this, set `FALSE` to filter `fresh/enable_theme_option`
